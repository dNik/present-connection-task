var list;
let pagesNumber = 0;
let currentPage = 1;
get()
$(document).on("click", "#main-list .table-element", function (e) {
  let id = $(this).attr('value');
  window.location.href = 'details_page.html?id=' + id;
});


$(document).on("click", ".pagination a", function () {
  $('.pagination').children().removeClass('active');
  $(this).addClass('active');
  showData($(this).attr('value'));
})

function get() {
  $.get("https://jsonplaceholder.typicode.com/posts", function (data, status) {

    if (status == 'success') {
      list = data;
      paging();
      showData(0);
    } else {
      alert('Api Error with status: ' + status);
    }

  });
}

function showData(nextPage) {

  currentPage = nextPage;
  if (currentPage == pagesNumber) {
    appendTable(currentPage * 10, null);
  }
  if (currentPage == 0) {
    appendTable(0, 10);
  } else {
    appendTable(currentPage * 10, currentPage * 10 + 10);
  }

}

function paging() {
  if (list.length % 10 == 0) {
    pagesNumber = list.length / 10;
  }
  else {
    pagesNumber = Math.trunc(list.length / 10) + 1;
  }
  var paging = $('.pagination');
  paging.append('<a href="#" value="0" class="active">1</a>');
  for (i = 2; i <= pagesNumber; i++) {
    paging.append('<a href="#" value="' + (i - 1) + '">' + i + '</a>');
  }
}

function appendTable(from, to) {
  $('#main-list tr').slice(1).remove();
  var subset;
  if (to == null) {
    subset = list.slice(from);
  } else {
    subset = list.slice(from, to);
  }
  subset.forEach(function (entry) {
    $('#main-list').append("<tr class='table-element' value=" + entry['id'] + "><td>" + entry['id'] + "</td><td>" + entry['title'] + "</td></tr>");
  });
}

