var url = new URL(document.URL);

let id = url.searchParams.get("id");
getById();

function getById(){
    $.get("https://jsonplaceholder.typicode.com/posts/" + id, function(data, status){

      if(status == 'success'){
        $('#details-id').append(data['id']);
        $('#details-userId').append(data['userId']);
        $('#details-title').append(data['title']);
        $('#details-body').append(data['body']);
      }else {
        alert('Api Error with status: ' + status);
      }
      
    });
}